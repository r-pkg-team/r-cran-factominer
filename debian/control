Source: r-cran-factominer
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Dylan Aïssi <daissi@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-factominer
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-factominer.git
Homepage: https://cran.r-project.org/package=FactoMineR
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-car,
               r-cran-cluster,
               r-cran-dt,
               r-cran-ellipse,
               r-cran-emmeans,
               r-cran-flashclust,
               r-cran-lattice,
               r-cran-leaps,
               r-cran-mass,
               r-cran-multcompview,
               r-cran-scatterplot3d,
               r-cran-ggplot2,
               r-cran-ggrepel
Testsuite: autopkgtest-pkg-r

Package: r-cran-factominer
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: Multivariate Exploratory Data Analysis and Data Mining
 Exploratory data analysis methods to summarize, visualize and describe
 datasets. The main principal component methods are available, those with
 the largest potential in terms of applications: principal component
 analysis (PCA) when variables are quantitative, correspondence analysis
 (CA) and multiple correspondence analysis (MCA) when variables are
 categorical, Multiple Factor Analysis when variables are structured in
 groups, etc. and hierarchical cluster analysis.
